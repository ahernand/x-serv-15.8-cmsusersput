from django.shortcuts import render
from django.http import HttpResponse
import sys
import operator

operaciones = {
    "sumar": operator.add,
    "restar": operator.sub,
    "multiplicar": operator.mul,
    "dividir": operator.truediv,
    }

def index(request):
    return HttpResponse("Hola, mundo. Estás en la página de inicio de tu app llamada calc.")

def op(request,op, op1, op2):

    #Realizamos la operacion
    try:
        resultado = operaciones[op](op1,op2)
    except ZeroDivisionError:
        return HttpResponse("No se puede dividir entre 0")

    return HttpResponse("El resultado es " + str(resultado))
    
